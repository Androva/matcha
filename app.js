// setup
// express app
const express = require("express");
const app = express();
const port = process.env.port || 3000;

// database
const Mongo = require("mongodb");
const MongoClient = require("mongodb").MongoClient;
const url = "mongodb://localhost:27017/matcha";
/* G */ global.db;

//required for use
const body_parser = require("body-parser");
const api_routes = require("./routes/api");

// connect uses (non db reliant)
app.use("/", express.static("public"));

app.use(body_parser.json());

// app functionality

// database connection && app initialisation
MongoClient.connect(url, { useNewUrlParser: true }, (err, database) => {
	if (err) console.log(err);
	else console.log("Mongo intialised on port 3000 ...");
	global.db = database.db("matcha");
	// db creation and user initialisation
	db.createCollection("users", function (err, res) {
		if (err) throw err;
		var users = [
			{
				username: "NalediMat",
				first_name: "Naledi",
				last_name: "Matutoane",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.104401199999998, -26.1973822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: "verified",
				profilePicture: "kj5j43h5jk3h4jk345.jpg",
				bio: "Blue da ba dee",
				dob: "1998-10-12",
				fame: 0,
				gender: "female",
				interests: ["memes", "games", "food"],
				likes: {
					"StaceyCool" : "blocked",
					"ThatChick" : "viewed",
					"FakePilot" : "liked",
					"ManOfYourDreams" : "connected"
				},
				preference: "both",
				profileViews: []
			},
			{
				username: "YaBoii",
				first_name: "Ya",
				last_name: "Boi",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.104401199999998, -25.9973822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902579695,
				profilePicture: "4kj5h34l53j;53.jpg",
				bio: "Just a small town boy livin' in South Detroit...",
				dob: "1985-01-05",
				fame: 0,
				gender: "male",
				interests: ["memes", "movies/series", "food"],
				likes: {},
				preference: "female",
				profileViews: []
			},
			{
				username: "SlayKween",
				first_name: "Slay",
				last_name: "Kween",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.124401199999998, -26.1983822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902579395,
				profilePicture: "45k6j464k56lj456.jpg",
				bio: "#slayforeva",
				dob: "1987-05-16",
				fame: 0,
				gender: "female",
				interests: ["travel", "food"],
				likes: {},
				preference: "male",
				profileViews: []
			},
			{
				username: "FakePilot",
				first_name: "Fake",
				last_name: "Pilot",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.204401199999998, -26.1973822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902479695,
				profilePicture: "c6494b0b1578901506382.jpeg",
				bio: "High in the sky...",
				dob: "1974-11-06",
				fame: 0,
				gender: "male",
				interests: ["travel", "food"],
				likes: {},
				preference: "both",
				profileViews: []
			},
			{
				username: "HappyGuy",
				first_name: "Happy",
				last_name: "Guy",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.104401129989998, -26.1873822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902579695,
				profilePicture: "j4h5g3453h4k5j32.jpg",
				bio: "Literally thee happipest guy you'll ever meet :)",
				dob: "1996-04-28",
				fame: 0,
				gender: "male",
				interests: ["memes", "food", "books"],
				likes: {},
				preference: "male",
				profileViews: []
			},
			{
				username: "ThatChick",
				first_name: "That",
				last_name: "Chick",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.104401199999998, -26.2973822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902578695,
				profilePicture: "180926140739_1_540x360.jpg",
				bio: "Heyyyyyyyyyyy ;)",
				dob: "1999-02-12",
				fame: 0,
				gender: "female",
				interests: ["books", "food"],
				likes: {},
				preference: "female",
				profileViews: []
			},
			{
				username: "JustAPerson",
				first_name: "Random",
				last_name: "Person",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.114401199999998, -26.2973822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902579691,
				profilePicture: "5645673485345.jpg",
				bio: "Person person person I am",
				dob: "1979-04-30",
				fame: 0,
				gender: "female",
				interests: ["books", "food", "travel"],
				likes: {},
				preference: "male",
				profileViews: []
			},
			{
				username: "JimmyMc",
				first_name: "Jim",
				last_name: "McDuff",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.103401199999998, -26.1873822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902578595,
				profilePicture: "jk4h5kj34h5k345.jpeg",
				bio: "I'm a really chill dude - get to know me before you judge me ;)",
				dob: "1970-09-16",
				fame: 0,
				gender: "male",
				interests: ["movies/series", "games", "travel"],
				likes: {},
				preference: "female",
				profileViews: []
			},
			{
				username: "StaceyCool",
				first_name: "Stacey",
				last_name: "Cormack",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.104401199999978, -26.1973822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902579695,
				profilePicture: "j45jl345jl34k5jl345j3.jpg",
				bio: "I'm a realy cool chic - let's go out some time and you can see for yourself!",
				dob: "1985-07-24",
				fame: 0,
				gender: "female",
				interests: ["movies/series", "food", "travel"],
				likes: {},
				preference: "both",
				profileViews: []
			},
			{
				username: "CalmLady",
				first_name: "Thembiwe",
				last_name: "Thandiso",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.104401199999978, -26.2873822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902568695,
				profilePicture: "h63k5j534lk5j353.jpg",
				bio: "I love love love cooking. Oh - and taking selfies XD",
				dob: "1983-10-02",
				fame: 0,
				gender: "female",
				interests: ["food", "travel"],
				likes: {},
				preference: "male",
				profileViews: []
			},
			{
				username: "ManOfYourDreams",
				first_name: "David",
				last_name: "Dicky",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.104401199999978, -26.2873822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902579325,
				profilePicture: "khkj5h5j65656456521.jpg",
				bio: "The best man you're going to find on this site.",
				dob: "2001-05-22",
				fame: 0,
				gender: "male",
				interests: ["food", "travel"],
				likes: {
					"NalediMat": "connected"
				},
				preference: "female",
				profileViews: []
			},
			{
				username: "DaniDani",
				first_name: "Daniella",
				last_name: "De Vito",
				email: "nmatutoa@student.wethinkcode.co.za",
				location: {
					type: "Point",
					coordinates: [28.10440129999998, -26.2973822]
				},
				password:
					"8112cecd0590e451ca2b1d70b47b4f9f441ca90569a1c481589e48a102dea124",
				status: 1578902579695,
				profilePicture: "k4jh5jk35h3k45.jpg",
				bio: "I like gaming and watching a tonne of Netflix - maybe we can hang sometime?",
				dob: "1990-12-25",
				fame: 0,
				gender: "female",
				interests: ["food", "games", "memes", "movies/series"],
				likes: {},
				preference: "female",
				profileViews: []
			},
		];
		db.collection("users").insertMany(users, function (err, res) {
			if (err) throw err;
			console.log("Number of users created: " + res.insertedCount);
		});
	});


	db.createCollection("images", function (err, res) {
		if (err) throw err
		var images = [
			{
				username: "NalediMat",
				filename: "kj5j43h5jk3h4jk345.jpg"
			},
			{
				username: "YaBoii",
				filename: "4kj5h34l53j;53.jpg"
			},
			{
				username: "SlayKween",
				filename: "45k6j464k56lj456.jpg"
			},
			{
				username: "FakePilot",
				filename: "c6494b0b1578901506382.jpeg"
			},
			{
				username: "HappyGuy",
				filename: "j4h5g3453h4k5j32.jpg"
			},
			{
				username: "ThatChick",
				filename: "180926140739_1_540x360.jpg"
			},
			{
				username: "JustAPerson",
				filename: "5645673485345.jpg"
			},
			{
				username: "JimmyMc",
				filename: "jk4h5kj34h5k345.jpeg"
			},
			{
				username: "StaceyCool",
				filename: "j45jl345jl34k5jl345j3.jpg"
			},
			{
				username: "CalmLady",
				filename: "h63k5j534lk5j353.jpg"
			},
			{
				username: "ManOfYourDreams",
				filename: "khkj5h5j65656456521.jpg"
			},
			{
				username: "DaniDani",
				filename: "k4jh5jk35h3k45.jpg"
			}
		];
		db.collection("images").insertMany(images, function (err, res) {
			if (err) throw err;
			console.log("Number of images inserted: " + res.insertedCount);
		});	
	});

	// app setup

	// app listen on succesful connection

	app.listen(port, function () {
		console.log("Listening on port 3000...");
	});

	// app use routes
	app.use("/api", api_routes);
});
