$(document).ready(function () {
    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function setCookie(cname, cvalue, exhours) {
        var d = new Date();
        d.setTime(d.getTime() + (exhours * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    var user = new Vue({
        el: "#content",
        data: {
            info: {}
        },
        beforeCreate: function () {
            function removeLoader() {
                $("#loader").fadeOut("slow");
            };

            $('body').prepend('<div id="loader" style="width: 100vw; height: 100vh; z-index: 999; position: absolute;"></div>');
            $("#loader").load('../loader/loader.html');
            setTimeout(removeLoader, 2000);
        },
        created: function () {
            var username = "../api/users/" + getCookie("username");
            $.get(username, function (data, status) {
                user.info = data[0];
            });
        },
        mounted: function () {
            // $('#btnSnap').click(function () {
            // $("#modalSnap").fadeIn("fast");
            var video = document.getElementById('video');

            $(video).click(() => { video.play(); });
            // Get access to the camera!
            if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                // Not adding `{ audio: true }` since we only want video now
                navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
                    // video.src = window.URL.createObjectURL(stream);
                    video.srcObject = stream;
                    video.play();
                });
            }
            // Elements for taking the snapshot
            setTimeout(() => {
                var canvas = document.getElementById('canvas');
                var context = canvas.getContext('2d');
                var video = document.getElementById('video');
                document.getElementById("snap").addEventListener("click", function () {
                    video.pause();
                    context.drawImage(video, 0, 0, 240, 180);

                    $('#downloadFile').click((e) => {
                        var dataURL = canvas.toDataURL('image/png');
                        $('#downloadFile').attr('href', dataURL);
                    });
                });
            }, 500);
            $('#uploadForm').submit(function (e) {
                e.preventDefault();
                $('#uploadForm').ajaxSubmit({ // Accessible by including the script //cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js
                    data: { username: user.info.username },
                    success: function (response) {
                        var update = { profilePicture: response.filename };
                        user.info.profilePicture = response.filename;
                        $.ajax({
                            url: "/api/users/" + user.info.username,
                            type: "put",
                            data: JSON.stringify(update),
                            headers: {
                                'Content-Type': 'application/json'
                            },
                            dataType: 'json',
                            success: function (data) {
                                var snackbar = document.querySelector("#snackbar");
                                var dataSnack = {
                                    message: "Image successfully uploaded.",
                                    timeout: 2000
                                };
                                snackbar.MaterialSnackbar.showSnackbar(dataSnack);
                            }
                        })
                    }
                });
                //Very important line, it disable the page refresh.
                return false;
            });
            // });   

            document.getElementById("save").addEventListener("click", function () {
                var info = user.info,
                    dob = document.querySelector("#dob").value,
                    gender = document.querySelector("#gender"),
                    preference = document.querySelector("#preference"),
                    bio = document.querySelector("#bio").value;

                gender = gender.getElementsByTagName("input");
                Array.from(gender).forEach(item => {
                    if (item.checked)
                        gender = item.value;
                })

                preference = preference.getElementsByTagName("input");
                Array.from(preference).forEach(item => {
                    if (item.checked)
                        preference = item.value;
                })

                interests = $("#interests").find("input");
                var interestsList = [];
                Array.from(interests).forEach(item => {
                    if (item.checked)
                        interestsList.push(item.value)
                })

                if (!dob || !gender || !preference || !bio || !interests || !user.info.profilePicture) {
                    var snackbar = document.querySelector("#snackbar");
                    var data = {
                        message: "Please fill in all required fields.",
                        timeout: 2000
                    };
                    snackbar.MaterialSnackbar.showSnackbar(data);
                }
                else {
                    var update = {
                        dob: dob,
                        gender: gender,
                        preference: preference,
                        bio: bio,
                        interests: interestsList,
                        likes: {},
                        profileViews: [],
                        fame: 0,
                    };

                    $.ajax({
                        url: "/api/users/" + getCookie("username"),
                        type: "put",
                        data: JSON.stringify(update),
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        dataType: 'json',
                        success: function (data) {
                            window.location.replace("../");
                        }
                    });
                }
            });
        }
    })
})