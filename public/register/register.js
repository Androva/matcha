var regex_reg = {
	username: {
		regex: /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{5,29}$/igm,
		message: "Usernames can contain alphanumeric characters, underscores and periods. The username cannot contain spaces, start with a period nor end with a period. It must also not have more than one period sequentially. Max length is 30 characters."
	},
	first_name: {
		regex: /^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$|^$/,
		message: "First name can not contain special characters."
	},
	last_name: {
		regex: /^(([A-za-z]+[\s]{1}[A-za-z]+)|([A-Za-z]+))$|^$/,
		message: "Last name can not contain special characters."
	},
	email: {
		regex: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g,
		message: "Email address invalid."
	},
	password_one: {
		regex: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
		message: "Password must include: 1 Uppercase, 1 Lowercase and 1 Numeric. Minimum 6 characters."
	},
	password_two: {
		regex: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
		message: "Password must include: 1 Uppercase, 1 Lowercase and 1 Numeric. Minimum 6 characters."
	}
}

var regex_log = {
	username: {
		regex: /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{5,29}$/igm,
		message: "Usernames can contain characters a-z, 0-9, underscores and periods. The username cannot start with a period nor end with a period. It must also not have more than one period sequentially. Max length is 30 chars."
	},
	password: {
		regex: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/,
		message: "Password must include: 1 Uppercase, 1 Lowercase and 1 Numeric. Minimum 6 characters."
	},
}

var regex_forgot = {
	username: {
		regex: /^(?!.*\.\.)(?!.*\.$)[^\W][\w.]{5,29}$/igm,
		message: "Usernames can contain characters a-z, 0-9, underscores and periods. The username cannot start with a period nor end with a period. It must also not have more than one period sequentially. Max length is 30 chars."
	}
}

function setCookie(cname, cvalue, exhours) {
	var d = new Date();
	d.setTime(d.getTime() + (exhours * 60 * 60 * 1000));
	var expires = "expires=" + d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
};

var validateFormInput = (formJSON, regex) => {
	var snackbarContainer = document.querySelector('#input_alert');
	for (key in formJSON) {
		if (!formJSON[key].match(regex[key].regex)) {
			var data = { message: regex[key].message, timeout: 5000 };
			snackbarContainer.MaterialSnackbar.showSnackbar(data);
			return key;
		}
	}
	return true;
};

var validateRegister = (formJSON, fn) => {
	var isValid = validateFormInput(formJSON, regex_reg);
	if (isValid == true && formJSON.password_one.match(formJSON.password_two)) {
		$.get('/api/users/' + formJSON.username, (data) => {
			if (data.length === 0)
				fn(true);
			else {
				var snackbarContainer = document.querySelector('#input_alert');
				snackbarContainer.MaterialSnackbar.showSnackbar({ message: "Username already exists." });
			}
		});
	}
	fn(false);
};

var validateLogin = (formJSON, fn) => {
	var isValid = validateFormInput(formJSON, regex_log);
	if (isValid == true) {
		$.ajax({
			url: '/api/users/' + formJSON.username,
			type: 'post',
			data: JSON.stringify(formJSON),
			headers: {
				'Content-Type': 'application/json'
			},
			dataType: 'json',
			success: function (data) {
				var snackbarContainer = document.querySelector('#input_alert');
				if (data.error) {
					snackbarContainer.MaterialSnackbar.showSnackbar({ message: data.message, timeout: 3500 });
					return;
				}
				if (data.res) {
					if (data.res.password === data.req.password) {
						if (data.res.status !== "reported") {
							if (data.res.status !== "registered")
								fn(true);
							else
								snackbarContainer.MaterialSnackbar.showSnackbar({ message: "Please verify your account with the email sent to: " + data.res.email, timeout: 3500 });
						} else
							snackbarContainer.MaterialSnackbar.showSnackbar({ message: "Your account has been reported and you are no longer permitted access to the site.", timeout: 3500 });
					} else
						snackbarContainer.MaterialSnackbar.showSnackbar({ message: "Password mismatch...", timeout: 3500 });
				} else
					snackbarContainer.MaterialSnackbar.showSnackbar({ message: "Username does not exist...", timeout: 3500 });
			}
		});
	}
};

var validateForgotPass = (formJSON, fn) => {
	var isValid = validateFormInput(formJSON, regex_forgot);
	if (isValid == true) {
		$.get('/api/users/' + formJSON.username, (data) => {
			if (data.length === 1) {
				fn(true);
			}
			else {
				var snackbarContainer = document.querySelector('#input_alert');
				snackbarContainer.MaterialSnackbar.showSnackbar({ message: "User cannot be found." });
				fn(false)
			}
		});
	}
	fn(false);
}

$.fn.serializeObject = function () {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function () {
		if (o[this.name]) {
			if (!o[this.name].push) {
				o[this.name] = [o[this.name]];
			}
			o[this.name].push($($.parseHTML(this.value)).text() || '');
		} else {
			o[this.name] = $($.parseHTML(this.value)).text() || '';
		}
	});
	return o;
};

var getLocation = (fn) => {
	navigator.geolocation.getCurrentPosition(
		(position) => {
			fn([position.coords.longitude, position.coords.latitude]);
		},
		(error) => {
			$.getJSON('https://ipinfo.io/json').done((location) => {
				fn(location.loc.split(',').map(item => parseFloat(item)));
			});
		}
	);
}

$(document).ready(function () {
	var users = new Vue({
		el: "#content",
		data: {
			user: {}
		},
		beforeCreate: () => {
			$('body').prepend('<div id="loader" style="width: 100vw; height: 100vh; position: absolute; z-index: 999;"></div>');
			$("#loader").load('../loader/loader.html');
			setTimeout(() => {
				$("#loader").remove();
			}, 2000);
		},
		mounted: () => {
			$('#submit_reg').click(() => {
				var formJSON = $('#form_reg').serializeObject();
				var isValid = validateRegister(formJSON, (isValid) => {
					if (isValid == true) {
						getLocation((location) => {
							formJSON['location'] = {
								type: "Point",
								coordinates: location
							};
							formJSON.password = formJSON.password_one;
							delete formJSON.password_one;
							delete formJSON.password_two;
							$.ajax({
								url: '/api/users',
								type: 'post',
								data: JSON.stringify(formJSON),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									var snackbarContainer = document.querySelector('#input_alert');
									snackbarContainer.MaterialSnackbar.showSnackbar({ message: "Success! Please verify your account with the email sent to: " + formJSON.email });
									setCookie("username", formJSON.username, 2);
									$.ajax({
										url: "/api/welcomeMail/send/" + formJSON.username,
										type: "post",
										data: JSON.stringify(formJSON),
										headers: {
											'Content-Type': 'application/json'
										},
										dataType: 'json'
									});
								}
							});
						});
					}
				});
			});

			$('#submit_log').click(() => {
				var formJSON = $('#form_log').serializeObject();
				validateLogin(formJSON, (isValid) => {
					getLocation((location) => {
						var json = {
							location: {
								type: "Point",
								coordinates: location
							},
							status: "online"
						};
						$.ajax({
							url: '/api/users/' + formJSON.username,
							type: 'put',
							data: JSON.stringify(json),
							headers: {
								'Content-Type': 'application/json'
							},
							dataType: 'json',
							success: function (data) {
								setCookie('username', formJSON.username, 3);
								window.location.replace('/');
							}
						});
					});
				});
			});

			$('#form_reg input').on('change', function () {
				if (validateFormInput($(this).serializeObject(), regex_reg) == true)
					$(this).css('border-bottom', '1px solid green');
				else
					$(this).css('border-bottom', '1px solid red');
				if ($(this).val() == "")
					$(this).css('border-bottom', '1px solid lightgrey');
			});

			$('#form_log input').on('change', function () {
				if (validateFormInput($(this).serializeObject(), regex_log) == true)
					$(this).css('border-bottom', '1px solid green');
				else
					$(this).css('border-bottom', '1px solid red');
				if ($(this).val() == "")
					$(this).css('border-bottom', '1px solid lightgrey');
			});

			$('#forgotPass').click(() => {
				$('#form_log').toggle()
				$('#submit_log').toggle()
				if ($('#forgotPassForm').is(":hidden")) {
					$('#forgotPass').html("Back to Login")
					$('#forgotPassForm').attr("hidden", false)
					$('#submitUsername').attr("hidden", false)
				}
				else {
					$('#forgotPassForm').attr("hidden", true)
					$('#submitUsername').attr("hidden", true)
					$('#forgotPass').html("Forgot Password")
				}
			})

			$('#submitUsername').click(() => {
				var formJSON = $('#forgotPassForm').serializeObject();
				var isValid = validateForgotPass(formJSON, (isValid) => {
					if (isValid == true) {
						$.ajax({
							url: '/api/forgotPassMail/send/' + formJSON.username,
							type: 'post',
							data: JSON.stringify(formJSON),
							headers: {
								'Content-Type': 'application/json'
							},
							dataType: 'json',
							success: function (data) {
								var snackbarContainer = document.querySelector('#input_alert');
								snackbarContainer.MaterialSnackbar.showSnackbar({ message: "We've sent an email to the email address linked to this account. Please check your inbox for further instructions." });
							},
							error: function (data) {
								var snackbarContainer = document.querySelector('#input_alert');
								snackbarContainer.MaterialSnackbar.showSnackbar({ message: "User cannot be found." });
							}
						});
					}
				});
			});
		}
	});
});