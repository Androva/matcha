$(document).ready(function () {

	var host = location.origin;
	Vue.filter('capitalize', function (value) {
		if (!value) return ''
			value = value.toString()
		return value.charAt(0).toUpperCase() + value.slice(1)
	})

	// Vue instance for entire index page
	var users = new Vue({
		el: "#content",
		data: {
			allUsers: {}, // Get request for all users. Excludes user logged in
			singleUser: {}, // Get request for user logged in,
			connectedUsers: [], // Object holding connected users (No way to filter in vue)
			viewedUsers: [], // Object holding viewed users (No way to filter in vue)
			likedUsers: [], // Object holding liked users (No way to filter in vue)
			likedByUser: [], // Object holding likedBy users (No way to filter in vue)
			modalUser: {}, // Get user info depending on who's info is requested. Info from allusers
			notifications: [], // Get all notifications for the user
			singleUserImages: [], // Get images for a user in modal
			filteredUsers: {}, // get filtered results
		},
		methods: {
			addFilter: function (checked, filter, type) {
				users.filteredUsers = users.allUsers;

				if (type == "Interest") {
					if (checked == true) {
						for (var index = 0; index < users.filteredUsers.length; index++) {
							var user = users.filteredUsers[index];

							if (!user.interests.includes(filter)) {
								users.filteredUsers.splice(index, 1)
								index--;
							}
						};
					}
				} 
				if (type == "Distance") {
					if (checked == true) {
						for (var index = 0; index < users.filteredUsers.length; index++) {
							var user = users.filteredUsers[index];
							var distance = parseInt(user.distance)
							if (!(distance >= filter[0] && distance <= filter[1])) {
								users.filteredUsers.splice(index, 1)
								index--;
							}
						}
					};
				}
				if (type == "Age") {
					if (checked == true) {
						for (index = 0; index < users.filteredUsers.length; index++) {
							var user = users.filteredUsers[index];
							var age = parseInt(user.dob)
							if (!(age >= filter[0] && age <= filter[1])) {
								users.filteredUsers.splice(index, 1)
								index--;
							}
						}
					}
				}
			}
		},
		beforeCreate: function () {
			function removeLoader() {
				$("#loader").fadeOut("slow");
			};

			$('body').prepend('<div id="loader" style="width: 100vw; height: 100vh; z-index: 999; position: absolute;"></div>');
			$("#loader").load('../loader/loader.html');
			setTimeout(removeLoader, 2000);
		},
		created: function () {
			$.get('api/users', function (data, status) {
				data.forEach((element, index) => {
					if (getCookie("username") == element.username) {
						users.singleUser = data.splice(index, 1)[0];
						users.singleUser.profilePicture = host + "/uploads/" + users.singleUser.profilePicture;
						// Move viewed, liked, connected users into relevant variables
						users.viewedUsers = [];
						users.likedUsers = [];
						users.connectedUsers = []; // Clear variables
						users.likedByUser = [];
						for (var key in users.singleUser.likes) {
							if (users.singleUser.likes.hasOwnProperty(key)) {
								if (users.singleUser.likes[key] == "viewed")
									users.viewedUsers.push(key);
								if (users.singleUser.likes[key] == "liked")
									users.likedUsers.push(key);
								if (users.singleUser.likes[key] == "connected")
									users.connectedUsers.push(key);
								if (users.singleUser.likes[key] == "likedBy")
									users.likedByUser.push(key);
							}
						};
					}
				});
				users.allUsers = data;
				users.filteredUsers = data
			});
		},
		mounted: function () {			
			var currentUser = getCookie('username');

			setInterval(function () {
				// Fetch user notifications
				$.get("api/notifications/" + currentUser, function (data, status) {
					users.notifications = data;
					$("#notifications").attr("data-badge", data.length);
				});
			}, 1000);

			setTimeout(function () { $("#refresh").trigger("click") }, 2000);

			// Regularly fetch data
			$("#refresh").click(function () {

				var filters = document.querySelectorAll(".mdl-switch")
				filters.forEach(checkbox => {
					if (checkbox.childNodes[0].checked == true) {
						checkbox.childNodes[0].checked = false
					}
				});

				$.get("/api/images/" + users.singleUser.username, function (data) {
					data.forEach(element => {
						element.filename = host + "/uploads/" + element.filename;
					});
					users.singleUserImages = data;
				})

				// Store currentUser username
				$.get('api/users', function (data, status) {
					// Process singleUser
					data.forEach((element, index) => {
						element.viewable = true;
						if (currentUser == element.username) {
							users.singleUser = element;
							element.viewable = false;
							// Adding host location for where image is saved
							users.singleUser.profilePicture = host + "/uploads/" + users.singleUser.profilePicture;
							users.viewedUsers = [];
							users.likedUsers = [];
							users.connectedUsers = [];
							users.likedByUser = [];
							for (var key in users.singleUser.likes) {
								if (users.singleUser.likes.hasOwnProperty(key)) {
									if (users.singleUser.likes[key] == "viewed")
										users.viewedUsers.push(key);
									if (users.singleUser.likes[key] == "liked")
										users.likedUsers.push(key);
									if (users.singleUser.likes[key] == "connected")
										users.connectedUsers.push(key);
									if (users.singleUser.likes[key] == "likedBy")
										users.likedByUser.push(key)
								}
							};
						}
					});

					// Process allUsers
					data.forEach((element, index) => {
						// Matching
						var rating = 0;
						// 1. Pref
						if (users.singleUser.preference == element.gender || users.singleUser.preference == "both") {
							if (element.preference == users.singleUser.gender || element.preference == "both")
								rating = 1;
						}
						if (rating == 1) {
							// 2. Interests
							if (users.singleUser.interests != [] && element.interests != []) {
								users.singleUser.interests.forEach((userInterest) => {
									element.interests.forEach((elementInterest) => {
										if (userInterest == elementInterest)
											rating += 1;
									});
								});
							}
							// 3. Fame
							if (element.fame - users.singleUser.fame > 0)
								rating *= (element.fame - users.singleUser.fame);
							else
								rating /= Math.abs(element.fame - users.singleUser.fame);
							// 4. Distance
							rating = (rating * 10.2);
							if (element.distance > 0)
								rating /= element.distance;
							element.rating = Math.round(rating * 100) / 100;
						}
						else {
							element.viewable = false;
						}

						// Remove blocked / reported users
						if ((element.username in users.singleUser.likes && users.singleUser.likes[element.username] == "blocked") || element.status == "reported")
							element.viewable = false;
						
						// Remove users that have blocked them
						if (element.likes[users.singleUser.username] != undefined) {
							if (element.likes[users.singleUser.username] == "blocked") {
								element.viewable = false
							}
						}

						// Calculate DoB
						var date = new Date(element.dob),
							ageDifMs = Date.now() - date.getTime(),
							ageDate = new Date(ageDifMs);
						element.dob = Math.abs(ageDate.getUTCFullYear() - 1970);
						if (element.status != "online") {
							var date = new Date(element.status),
								lastSeen = date.toLocaleDateString(),
								today = new Date();
							today = today.toLocaleDateString();
							if (today == lastSeen)
								lastSeen = "Today (" + date.toLocaleTimeString() + ")";
							element.status = lastSeen;
						}

						// Calculate distance from singleUser (rounded to nearest km)
						element.distance = Math.round(getDistance(
							users.singleUser.location.coordinates[1],	// singleUser lat
							users.singleUser.location.coordinates[0],	// singleUser lon
							element.location.coordinates[1],			// element lat
							element.location.coordinates[0]				// element lon
						));
						if (element.distance > 20)
							element.viewable = false
					});

					// Assign computed data to allUsers
					users.allUsers = [];
					data.forEach((element, index) => {
						if (element.viewable != false)
							users.allUsers.push(element);
					})

					users.allUsers.sort(function (a, b) {
						adist = Math.round(a.distance / 15);
						bdist = Math.round(b.distance / 15);
						if (adist != bdist)
							return (adist - bdist);
						return (b.rating - a.rating);
					});
					users.filteredUsers = users.allUsers
				});
			});

			// Scroll chat to latest message on input focus
			$('#message_text').focusin(() => {
				setChatScroll();
			});
			// Trigger message send when enter key is pressed
			$('#message_text').keypress((e) => {
				if (e.which == 13)
					$('#chat_submit').click();
			})
			// Send a message; Send notification; Push message to list;
			$('#chat_submit').click((e) => {
				if ($('#message_text').val() != "") {
					var username = $('.chat_user.active')[0].innerText;
					var chat_name;
					if (username > users.singleUser.username)
						chat_name = username + "." + users.singleUser.username;
					else
						chat_name = users.singleUser.username + "." + username;
					var message = {
						"time": Date.now(),
						"message": $('#message_text').val(),
						"sender": users.singleUser.username
					};
					$.ajax({
						url: '/api/chat/' + chat_name,
						type: 'put',
						data: JSON.stringify(message),
						headers: {
							'Content-Type': 'application/json'
						},
						dataType: 'json',
						success: () => {
							$(".waiting").removeClass('waiting');
						}
					});
					$.ajax({
						url: '/api/notifications/',
						type: 'post',
						data: JSON.stringify({
							for: username,
							from: users.singleUser.username,
							notification: "You have received a message from " + users.singleUser.username + ".",
							type: "message"
						}),
						headers: {
							'Content-Type': 'application/json'
						},
						dataType: 'json'
					});
					loadMessage(message, 'waiting');
					$('#message_text').val("");
					$('#message_text').focus();
				}
			});

			// Close button for modals
			document.querySelector("#modalClose").addEventListener('click', function () {
				$("#modal").fadeOut("fast");
			})

			// Logout button. Destroys cookies and redirects to login
			document.querySelector("#logout").addEventListener("click", function () {
				var cookies = document.cookie.split(";");

				for (var i = 0; i < cookies.length; i++) {
					var cookie = cookies[i];
					var eqPos = cookie.indexOf("=");
					var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
					document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
				}
				var json = {
					status: Date.now()
				}
				$.ajax({
					url: '/api/users/' + users.singleUser.username,
					type: 'put',
					data: JSON.stringify(json),
					headers: {
						'Content-Type': 'application/json'
					},
					dataType: 'json',
					success: function (data) {
						window.location = "../register/";
					}
				});
			});

			setTimeout(function () {
				$("#closeSnap").click(function () {
					$("#modalSnap").fadeOut();
				})
				$('#btnSnap').click(function () {
					// Fetch singleUser images
					$.get("/api/images/" + users.singleUser.username, function (data) {
						data.forEach(element => {
							element.filename = host + "/uploads/" + element.filename;
						});
						users.singleUserImages = data;
					})
					var images = document.getElementsByClassName("images");
					Array.from(images).forEach(element => {
						element.addEventListener('click', function () {
							var update = { profilePicture: element.src.replace(host + "/uploads/", '') }
							$.ajax({
								url: '/api/users/' + users.singleUser.username,
								type: 'put',
								data: JSON.stringify(update),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									var snackbar = document.querySelector("#snackbar");
									var data = {
										message: "Profile picture successfully updated."
									};
									snackbar.MaterialSnackbar.showSnackbar(data);
								}
							})
						})
					});

					$("#modalSnap").fadeIn("fast");
					var video = document.getElementById('video');

					$(video).click(() => { video.play(); });
					if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
						navigator.mediaDevices.getUserMedia({ video: true }).then(function (stream) {
							video.srcObject = stream;
							video.play();
						});
					}
					// Elements for taking the snapshot
					var canvas = document.getElementById('canvas');
					var context = canvas.getContext('2d');
					var video = document.getElementById('video');
					document.getElementById("snap").addEventListener("click", function () {
						video.pause();
						context.drawImage(video, 0, 0, 320, 240);

						$('#downloadFile').click((e) => {
							var dataURL = canvas.toDataURL('image/png');
							$('#downloadFile').attr('href', dataURL);
						});
					});

					$('#uploadForm').submit(function (e) {
						e.preventDefault();
						$('#uploadForm').ajaxSubmit({ // Accessible by including the script //cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.min.js
							data: { username: users.singleUser.username },
							success: function (response) {
								var snackbar = document.querySelector("#snackbar");
								var data = {
									message: "Image successfully uploaded."
								};
								snackbar.MaterialSnackbar.showSnackbar(data);
							}
						});
						//Very important line, it disables the page refresh.
						return false;
					});
				});
			}, 1000);

			// Set chat to hidden initially
			$('.chat_service_hideable').hide();
			setTimeout(() => {
				// Create notification loop to check for messages (count)
				notificationsLoop = setNotificationLoop(users.singleUser.username, "message");
				// Swap open and closed states to control interval loops (chat loop (fetch) / notification loop (count))
				$('#chat_control').click(function (el) {
					if ($(this).attr('state') == "open") {
						clearInterval(chatLoop);
						notificationsLoop = setNotificationLoop(users.singleUser.username, "message");
						$(this).attr('state', 'closed');
						$('.chat_service_hideable').hide();
					} else {
						$.get('/api/notifications/delete/' + users.singleUser.username + '?type=message');
						clearInterval(notificationsLoop);
						$(this).attr('state', 'open');
						$('.chat_service_hideable').show();
					}
				});

				// Eventlistener to get modal with user info
				var info = document.querySelectorAll(".card #info");
				Array.from(info).forEach(item => {
					item.addEventListener('click', function () {
						var user = $(item.parentNode).attr("user");
						users.allUsers.forEach((element, index) => {
							if (user == element.username)
								users.modalUser = element;
						});
						var status = document.getElementById("infoStatus");
						if (users.modalUser.username == $(status).attr("user")) {
							if (users.modalUser.status == "online")
								status.src = "https://img.icons8.com/material/100/4CAF50/new-moon.png";
							else
								status.src = "https://img.icons8.com/material/100/9E9E9E/new-moon.png";
						}
						$.get("/api/images/" + users.modalUser.username, function (data) {
							if (data) {
								users.modalUser.images = data;
							}
						})
					});
				});

				// Liking a user. Disallows liking the same user more than once and connects users who have liked each other
				var like = document.querySelectorAll('.card #like');
				Array.from(like).forEach(item => {
					var username = $(item.parentNode).attr("user");
					if (users.singleUser.likes && (users.singleUser.likes[username] == "liked" || users.singleUser.likes[username] == "connected"))
						item.childNodes[0].innerHTML = "favorite";
					else
						item.childNodes[0].innerHTML = "favorite_border";

					$(item).click(function () {
						var username = $(item.parentNode).attr("user");
						var sUserLikes = users.singleUser.likes;
						let likedUser = users.allUsers.find(likedUser => likedUser.username == username);
						var like = likedUser.likes;
						if (!sUserLikes || sUserLikes == null)
							sUserLikes = {};
						// Disliking a user
						if (sUserLikes[username] == "liked" || sUserLikes[username] == "connected") {
							if (sUserLikes[username] == "connected")
								like[users.singleUser.username] = "liked";
							delete sUserLikes[username];
							var update = {
								likes: like,
								fame: likedUser.fame - 1
							};
							$.ajax({
								url: "api/users/" + username,
								type: "put",
								data: JSON.stringify(update),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									$.ajax({
										url: '/api/notifications/',
										type: 'post',
										data: JSON.stringify({
											for: username,
											from: users.singleUser.username,
											notification: users.singleUser.username + " has disliked you",
											type: "like"
										}),
										headers: {
											'Content-Type': 'application/json'
										},
										dataType: 'json'
									});
								}
							});
							var update = { likes: sUserLikes };
							$.ajax({
								url: "api/users/" + getCookie("username"),
								type: "put",
								data: JSON.stringify(update),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									item.childNodes[0].innerHTML = "favorite_border";
								}
							});
						}
						// Liking a user
						else {
							// Connecting with a mutually liked user
							if (like && users.singleUser.username in like) {
								sUserLikes[username] = "connected";
								like[users.singleUser.username] = "connected";
								var update = {
									likes: like,
									fame: likedUser.fame + 2
								};
								$.ajax({
									url: "api/users/" + username,
									type: "put",
									data: JSON.stringify(update),
									headers: {
										'Content-Type': 'application/json'
									},
									dataType: 'json'
								});
								$.ajax({
									url: '/api/notifications/',
									type: 'post',
									data: JSON.stringify({
										for: username,
										from: users.singleUser.username,
										notification: users.singleUser.username + " has liked you back! You are now connected and may chat",
										type: "like"
									}),
									headers: {
										'Content-Type': 'application/json'
									},
									dataType: 'json'
								});
							}
							else {
								sUserLikes[username] = "liked";
								like[users.singleUser.username] = "likedBy"
								$.ajax({
									url: 'api/users/' + username,
									type: 'put',
									data: JSON.stringify({
										fame: likedUser.fame + 2,
										likes: like
									}),
									headers: {
										'Content-Type': 'application/json'
									},
									dataType: 'json'
								});
								$.ajax({
									url: '/api/notifications/',
									type: 'post',
									data: JSON.stringify({
										for: username,
										from: users.singleUser.username,
										notification: users.singleUser.username + " has liked you!",
										type: "like"
									}),
									headers: {
										'Content-Type': 'application/json'
									},
									dataType: 'json'
								});
							}
							var update = { likes: sUserLikes };
							$.ajax({
								url: "/api/users/" + getCookie("username"),
								type: "put",
								data: JSON.stringify(update),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									item.childNodes[0].innerHTML = "favorite";
								}
							});
						}
					});
				});

				// Eventlistener to block a user
				var block = document.querySelectorAll(".card #block");
				Array.from(block).forEach(item => {
					item.addEventListener('click', function () {
						var user = $(item.parentNode).attr("user"),
							likes = users.singleUser.likes;
						likes[user] = "blocked";
						$.ajax({
							url: "api/users/" + users.singleUser.username,
							type: "put",
							data: JSON.stringify({ likes: likes }),
							headers: {
								'Content-Type': 'application/json'
							},
							dataType: 'json',
							success: function (data) {
								var snackbar = document.querySelector("#homeSnackbar");
								var data = {
									message: user + " has been blocked. They will no longer show up in your profile searches.",
									timeout: 3500
								};
								snackbar.MaterialSnackbar.showSnackbar(data);
							}
						});
					})
				});

				// Eventlistener to report a user
				var report = document.querySelectorAll(".card #report");
				Array.from(report).forEach(item => {
					item.addEventListener('click', function () {
						var user = $(item.parentNode).attr("user"),
							likes = users.singleUser.likes;
						likes[user] = "reported";
						$.ajax({
							url: "api/users/" + users.singleUser.username,
							type: "put",
							data: JSON.stringify({ likes: likes }),
							headers: {
								'Content-Type': 'application/json'
							},
							dataType: 'json',
							success: function (data) {
								$.ajax({
									url: "/api/users/" + user,
									type: "put",
									data: JSON.stringify({ status: "reported" }),
									headers: {
										'Content-Type': 'application/json'
									},
									dataType: 'json',
									success: function (data) {
										var snackbar = document.querySelector("#homeSnackbar");
										var data = {
											message: user + " has been reported. They will no longer be permitted access to this site.",
											timeout: 3500
										};
										snackbar.MaterialSnackbar.showSnackbar(data);
									}
								});
							}
						});
					})
				});

				$("#filterAndSort").click(() => {
					if ($('#sortAndFilter').is(":hidden"))
						$("#sortAndFilter").attr("hidden", false)
					else
						$("#sortAndFilter").attr("hidden", true)
					
				})

				// Update user info
				var textItems = new Array,
					gender = new Array,
					preference = new Array,
					interests = new Array;

				textItems.push(document.getElementById("email"));
				textItems.push(document.getElementById("password"));
				textItems.push(document.getElementById("bio"));
				gender = document.getElementsByName("gender");
				preference = document.getElementsByName("preference");
				interests = document.getElementsByName("interests");

				// Username, email, password and bio change
				textItems.forEach(element => {
					element.addEventListener("change", function () {
						if (element.value != null && element.value != undefined && element.value) {
							var obj = {};
							obj[$(element).attr('name')] = $(element).val();
							$.ajax({
								url: "/api/users/" + getCookie("username"),
								type: "put",
								data: JSON.stringify(obj),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									var snackbar = document.querySelector("#snackbar");
									var data = { message: "Profile successfully updated" };
									snackbar.MaterialSnackbar.showSnackbar(data);
								}
							});
						}
					});
				});

				// Change Gender
				gender.forEach(element => {
					element.addEventListener("change", function () {
						if (element.value != null && element.value != undefined && element.value) {
							var obj = {};
							obj[$(element).attr('name')] = $(element).val();
							$.ajax({
								url: "api/users/" + getCookie("username"),
								type: "put",
								data: JSON.stringify(obj),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									var snackbar = document.querySelector("#snackbar");
									var data = { message: "Profile successfully updated" };
									snackbar.MaterialSnackbar.showSnackbar(data);
								}
							});
						}
					})
				})

				// Change Preference
				preference.forEach(element => {
					element.addEventListener("change", function () {
						if (element.value != null && element.value != undefined && element.value) {
							var obj = {};
							obj[$(element).attr('name')] = $(element).val();
							$.ajax({
								url: "api/users/" + getCookie("username"),
								type: "put",
								data: JSON.stringify(obj),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									var snackbar = document.querySelector("#snackbar");
									var data = { message: "Profile successfully updated" };
									snackbar.MaterialSnackbar.showSnackbar(data);
								}
							});
						}
					})
				})

				// Change Interests
				interests.forEach(element => {
					element.addEventListener("change", function () {
						var list = new Array;
						Array.from(document.getElementsByName("interests")).forEach(element => {
							if (element.checked)
								list.push(element.value);
						});
						if (list.length < 1) {
							var snackbar = document.querySelector("#snackbar"),
								data = { message: "Please select at least one interest" };
							snackbar.MaterialSnackbar.showSnackbar(data);
						} else {
							var obj = {};
							obj[$(element).attr('name')] = list;
							$.ajax({
								url: "api/users/" + getCookie("username"),
								type: "put",
								data: JSON.stringify(obj),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
								success: function (data) {
									var snackbar = document.querySelector("#snackbar"),
										data = { message: "Profile successfully updated" };
									snackbar.MaterialSnackbar.showSnackbar(data);
								}
							});
						}
					})
				})

				// Bring up user info modal on click of chips on "Interactions" page
				var chips = document.querySelectorAll(".mdl-chip");
				Array.from(chips).forEach(item => {
					item.addEventListener('click', function () {
						users.allUsers.forEach(element => {
							if (element.username == item.innerText)
								users.modalUser = element;
						});
						$.get("/api/images/" + users.modalUser.username, function (data) {
							if (data) {
								users.modalUser.images = data;
							}
						})
						$("#modal").fadeIn("fast");

						var status = document.getElementById("infoStatus");
						if (users.modalUser.status == "online")
							status.src = "https://img.icons8.com/material/100/4CAF50/new-moon.png";
						else
							status.src = "https://img.icons8.com/material/100/9E9E9E/new-moon.png";


						var views = users.modalUser.profileViews,
							fame = users.modalUser.fame + 1;
						if (!views.includes(users.singleUser.username)) {
							views.push(users.singleUser.username);
							url = "/api/users/" + user;
							$.ajax({
								url: "api/users/" + user,
								type: "put",
								data: JSON.stringify({
									profileViews: views,
									fame: fame
								}),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
							});
						}
					})
				});
			}, 200);
		},
		updated: function () {
			// Chat with user tabbing
			// On chat selection start chat loop
			var connected = Array.from(document.getElementsByClassName('chat_user'));
			connected.forEach((el) => {
				$(el).click(() => {
					$('.chat_user').removeClass('active');
					$(el).addClass('active');
					var username = $(el).html();
					var chat_name;
					if (username > users.singleUser.username)
						chat_name = username + "." + users.singleUser.username;
					else
						chat_name = users.singleUser.username + "." + username;
					clearInterval(chatLoop);
					setChatLoop(chat_name, setChatScroll());
					chatLoop = setInterval(() => {
						return setChatLoop(chat_name);
					}, 2000);
				})
			});

			// Modal glider with user images
			var info = document.querySelectorAll(".card #info");
			Array.from(info).forEach(item => {
				item.addEventListener('click', function () {
					var user = $(item.parentNode).attr("user");
					users.allUsers.forEach((element, index) => {
						if (user == element.username)
							users.modalUser = element;
						var views = users.modalUser.profileViews,
							likes = users.singleUser.likes,
							fame = users.modalUser.fame;
						if (!fame)
							fame = 0;
						if (!likes[user]) {
							likes[user] = "viewed";
							$.ajax({
								url: "api/users/" + users.singleUser.username,
								type: "put",
								data: JSON.stringify({
									likes: likes
								}),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
							});
						}
						if (!views)
							views = new Array;
						if (!views.includes(users.singleUser.username)) {
							views.push(users.singleUser.username);
							url = "/api/users/" + user;
							$.ajax({
								url: "api/users/" + user,
								type: "put",
								data: JSON.stringify({
									profileViews: views,
									fame: fame + 1
								}),
								headers: {
									'Content-Type': 'application/json'
								},
								dataType: 'json',
							});
						}
					});
					$("#modal").fadeIn("fast");
				});
			});

			// Check off user info in profile page
			var email = document.getElementById("email_label"),
				gender = document.getElementById("gender"),
				preference = document.getElementById("preference"),
				bio = document.getElementById("bio_label"),
				interests = document.getElementById("interests");
			email.innerText = users.singleUser.email;
			bio.innerText = users.singleUser.bio;
			Array.from(gender.getElementsByTagName("input")).forEach(item => {
				if (item.value) {
					if (item.value == users.singleUser.gender)
						item.checked = true;
				}
			})
			Array.from(preference.getElementsByTagName("input")).forEach(item => {
				if (item.value) {
					if (item.value == users.singleUser.preference)
						item.checked = true;
				}
			})
			Array.from(interests.childNodes).forEach(item => {
				if (users.singleUser.interests.includes(item.value)) {
					item.checked = true;
				}
			})

			// Deleting notifications
			var notifications = document.querySelector("#sidebar").getElementsByTagName("p");
			Array.from(notifications).forEach(element => {
				$(element).hover(function () {
					element.style.backgroundColor = "rgba(255, 255, 255, 0.2)";
					element.style.cursor = "pointer";
				})
				$(element).mouseleave(function () {
					element.style.backgroundColor = "transparent";
				})
				element.addEventListener("click", function () {
					$.ajax({
						url: "api/notifications/" + $(element).attr("id"),
						type: "delete",
						headers: {
							'Content-Type': 'application/json'
						}
					});
				})
			})
		}
	});

	// Get cookies
	function getCookie(cname) {
		var name = cname + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for (var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
			}
		}
		return "";
	}

	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}

	if (getCookie("username") == null || !getCookie("username"))
		window.location = "../register";

	function deg2rad(deg) {
		return deg * (Math.PI / 180)
	}

	function getDistance(lat1, lon1, lat2, lon2) {
		var R = 6371; // Radius of the earth in km
		var dLat = deg2rad(lat2 - lat1);  // Conversion to radians
		var dLon = deg2rad(lon2 - lon1);
		var a =
			Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
			Math.sin(dLon / 2) * Math.sin(dLon / 2)
			;
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		var d = R * c; // Distance in km
		return d;
	}

	// Set's chat scroll to bottom
	var setChatScroll = () => {
		var element = document.getElementById("chat_content");
		element.scrollTop = element.scrollHeight;
	}
	// Add message object to front end with optional classes
	var loadMessage = (message, additional = null) => {
		var today = new Date();
		var msg = document.createElement('div');
		var msg_time = new Date(message.time);
		if (today.getDay() != msg_time.getDay())
			msg_time = msg_time.toLocaleDateString();
		else
			msg_time = msg_time.toLocaleTimeString();
		$(msg).addClass(message.sender === users.singleUser.username ? 'msg_left' : 'msg_right');
		if (additional != null)
			$(msg).addClass(additional);
		$(msg).append('<span>' + message.message + '</span>')
		$(msg).append('<br><small>' + msg_time + '</small>');
		$('#chat_content').append(msg);
	};
	//Notification loop that has an optional type parameter
	var notificationsLoop;
	var setNotificationLoop = (username, type = null) => {
		var query = username;
		if (type != null)
			query = query + '?type=' + type;
		return setInterval(() => {
			$.get('/api/notifications/count/' + query, (result) => {
				$('#chat_control span').attr('data-badge', result.count);
			});
		}, 1000);
	};
	// Chat message loop to fetch chat json
	var chatLoop;
	var setChatLoop = (chat_name) => {
		$.get('/api/chat/' + chat_name, (result) => {
			if (result == "") {
				$.ajax({
					url: '/api/chat',
					type: 'post',
					data: JSON.stringify({
						"chathook": chat_name,
						"messages": []
					}),
					headers: {
						'Content-Type': 'application/json'
					},
					dataType: 'json'
				});
			} else {
				$('#chat_content').empty();
				result.messages.forEach((message) => {
					loadMessage(message);
				});
			}
		});
	}
});