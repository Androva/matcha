# matcha

A dating site created using the MEVN (Mongodb, Express, Vue, Node) stack. Vue.js knowledge was limited and as such, was not used to it's utmost potential. 

Matcha is a dating web app designed to match users based on gender, sexual preference, geolocation and common interests. Each user is assigned a 'fame rating' (calculated by how many profile views, likes and dislikes a user obtains. Each user profile displays a bio, range of photos and names. 

Matcha provides a live chat for connected (mutually liked) users to communicate. Users can block and report others, as well as change a few of their profile details. 

Also has a simple account verification method.

