const express = require("express");
const router = express.Router();
const multer  = require('multer');
const crypto = require('crypto');
const mime = require('mime');
const hash = 'sha256';
const nodemailer = require('nodemailer');
const mongodb = require('mongodb');
const transporter = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: 'naledimatutoane@gmail.com',
        pass: 'ltvoxxlehkuwsbnq'
    }
});

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/uploads/')
    },
    filename: function (req, file, cb) {
        crypto.pseudoRandomBytes(4, function (err, raw) {
                cb(null, raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
        });
    }
});
var upload = multer({ storage: storage });

function randomString() {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz.!@$#&*";
	var string_length = 10;
	var randomstring = '';
	for (var i = 0; i < string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum + 1);
	}
	randomstring += "!0";
	return randomstring;
}

// Get all users
router.get('/users', function (req, res, next){
    var db = global.db;
	var query = {};
    db.collection("users").find().toArray(function (err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);
    });
});

// Get single user with id
router.get('/users/:username', function (req, res, next){
    var db = global.db;
    var query = { username: req.params.username}
    db.collection("users").find(query).toArray(function (err, result) {
        if (err)
            res.send({ message: "User not found...", error: err });
        else
            res.send(result);
    });
});

// Post new user
router.post('/users', function (req, res, next){
    var db = global.db;
    req.body.password = crypto.createHash(hash).update(req.body.password).digest('hex');
    req.body.status = 'registered';
    db.collection("users").insertOne(req.body, function(err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);            
    });
});

// Login
router.post('/users/:username', function (req, res, next){
    var db = global.db;
    var query = { username: req.params.username}
    db.collection("users").find(query).toArray(function (err, result) {
        if (err)
            res.send({ message: "User not found...", error: err });
        else{
            req.body.password = crypto.createHash(hash).update(req.body.password).digest('hex');
            res.send({'res': result[0], 'req': req.body});
        }            
    });
});

// Update user with username
router.put('/users/:username', function (req, res, next){
    var db = global.db;
    var query = { username: req.params.username };
    if (req.body.password)
        req.body.password = crypto.createHash(hash).update(req.body.password).digest('hex');
    var newVal = { $set: req.body };
    db.collection("users").updateOne(query, newVal, function (err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);
    });
});

// Delete user with id
router.delete('/users/:username', function (req, res, next){
    var db = global.db;
    var query = { username: req.params.username };
    db.collection("users").deleteOne(query, function (err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);
    });
});

router.get('/images/', function (req, res, next){
    var db = global.db;
    db.collection("images").find({}).toArray(function (err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);
    });
});

router.get('/images/:name', function (req, res, next){
    var db = global.db;
    db.collection("images").find({username: req.params.name}).toArray(function (err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);
    });
});

router.post("/images/", function (req, res, next) {
    var db = global.db;
    db.collection("images").insertOne(req.body, function(err, result) {
        if (err)
            res.send(err)
        else
            res.send({ filename: req.body });
    });
});

router.post('/images/upload', upload.single('avatar'), function (req, res, next) {
    var db = global.db;
    req.body['filename'] = req.file.filename;
    db.collection("images").insertOne(req.body, function(err, result) {
        if (err)
            res.send(err)
        else
            res.send({filename: req.file.filename});
    });
});

router.delete('/delete/:filename', function (req, res, next){
    var db = global.db;
    var query = { filename: req.params.filename };
    db.collection("images").deleteOne(query, function (err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);
    });
});

router.delete('/delete/:_id', function (req, res, next){
    var db = global.db;
    var query = { _id: req.params._id };
    db.collection("images").deleteOne(query, function (err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);
    });
});

router.post('/welcomeMail/send/:username', function (req, res) {
    var userhash = crypto.createHash(hash).update(req.params.username).digest('hex');
    var username = encodeURIComponent(Buffer.from(req.params.username).toString('base64'));

    var mailOptions = {
        from: 'naledimatutoane@gmail.com',
        to: req.body.email,
        subject: 'Welcome to Matcha!',
        text: 'Welcome to matcha, please view in html to follow link...',
        html: '<h1> Welcome to Matcha! </h1> <br/> <p>To verify your account, please click on the link below: <br> <a href="http://' + req.get('host') + '/api/mail/verify/' + req.params.username + '?userhash=' + userhash + '&user=' + username + '">Verify account!</a> </p>'
    }
    transporter.sendMail(mailOptions, (err, response) => {
        if (err)
            response.send(err);
        else
            response.send(res);
    });
});

router.post('/forgotPassMail/send/:username', function (req, res) {
	var tempPass = randomString();
	var db = global.db;
	
	db.collection("users").find(req.body).toArray(function (err, result) {
		if (err)
			res.send({ message: "Email for user could not be found", error: err });
		else {
			var email = (result[0].email)
			var query = req.body
			var mailOptions = {
				from: "Matcha",
				to: email,
				subject: "Oops - you forgot your password!",
				text: "Instructions to reset your password inside",
				html: '<h1> It seems you\'ve forgotten your password for Matcha. </h1> <p>This is a temporary password: <br/><br/><b>' + tempPass + '</b><br/> We strongly recommend that you change it at your next account login. </p><br/><br/> Happy Matching!'
			}
			
			transporter.sendMail(mailOptions, (err, response) => {
				if (err)
					res.send(err)
				else {
					var hashPass = crypto.createHash(hash).update(tempPass).digest('hex');
					db.collection("users").updateOne(query, { $set: { password: hashPass } }, function (error, result) {
						if (error) throw error
						res.send(result)
					})
				}
			})
		}
	})
    
})

router.get('/mail/verify/:username', function (req, res) {
    var userhash = crypto.createHash(hash).update(req.params.username).digest('hex');
    var testhash = req.query.userhash;
    if (testhash){
        if (testhash === userhash){
            var query = { username: req.params.username };
            var newVal = { $set: { status: 'verified' } };
            db.collection("users").updateOne(query, newVal, function (err, result) {
                if (err)
                    res.send({ message: err.errmsg, error: err.name, code: err.code})
                else
                    res.redirect('/profile/');
            });
        }
    }else
        res.status(404).send({status: "Error", message: "Invalid verification."});
});

router.get('/chat/', function (req, res){
    db.collection("chats").find({}).toArray(function (err, result) {
        if (err)
            res.send(err);
        else
            res.send(result);
    });
});

router.get('/chat/:chathook', function (req, res){
    var db = global.db;
    var query = { chathook: req.params.chathook };
    db.collection("chats").findOne(query, function (err, result) {
        if (err)
            res.send(err);
        else
            res.send(result);
    });
});

router.put('/chat/:chathook', function (req, res){
    var db = global.db;
    var query = { chathook: req.params.chathook };
    var newVal = { 
        $push: {
            messages: req.body
        }
    };
    db.collection("chats").updateOne(query, newVal, function (err, result) {
        if (err)
            res.send(err)
        else
            res.send(result);
    });
});

router.post('/chat/', function (req, res, next) {
    var db = global.db;
    db.collection("chats").insertOne(req.body, function(err, result) {
        if (err)
            res.send(err)
        else
            res.send(result);
    });
    // res.send(req.body);
});

router.get('/notifications/', function (req, res){
    var db = global.db;
    db.collection("notifications").find({}).toArray(function(err, result) {
        if (err)
            res.send(err)
        else
            res.send(result);
    });
});

router.get('/notifications/:username', function (req, res, next) {
    var db = global.db;
    var query = {for: req.params.username};
    if (req.query.type)
        query.type = req.query.type;
    db.collection("notifications").find(query).toArray(function(err, result) {
        if (err)
            res.send(err)
        else{
            res.send(result);
            // db.collection('notifications').remove(query);
        }
    });
});

router.get('/notifications/delete/:username', function (req, res, next) {
    var db = global.db;
    var query = {for: req.params.username};
    if (req.query.type)
        query.type = req.query.type;
    db.collection("notifications").find(query).toArray(function(err, result) {
        if (err)
            res.send(err)
        else{
            res.send({status: "success"});
            db.collection('notifications').remove(query);
        }
    });
});

router.get('/notifications/count/:username', function (req, res, next) {
    var db = global.db;
    var query = {for: req.params.username};
    if (req.query.type)
        query.type = req.query.type;
    db.collection("notifications").countDocuments(query, function(err, result) {
        if (err)
            res.send(err);
        else
            res.send({count: result});
    });
    // res.send(req.body);
});

router.delete('/notifications/:id', function (req, res, next) {
    var db = global.db;
    var id = new mongodb.ObjectID(req.params.id);
    var query = { _id: id };
    db.collection("notifications").deleteOne(query, function (err, result) {
        if (err)
            res.send({ message: err.errmsg, error: err.name, code: err.code})
        else
            res.send(result);
    });
});


router.post('/notifications/', function (req, res, next) {
    var db = global.db;
    db.collection("notifications").insertOne(req.body, function(err, result) {
        if (err)
            res.send(err)
        else
            res.send(result);
    });
});

module.exports = router;